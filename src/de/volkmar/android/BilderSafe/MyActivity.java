/*
 * Copyright (c) 2013 Volkmar Vogel
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.volkmar.android.BilderSafe;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MyActivity extends Activity
{
    private ImageView ivBild;
	private EditText etPasswort;
	private Button btnOeffnen;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

		ivBild = (ImageView) findViewById(R.id.ivBild);
		etPasswort = (EditText) findViewById(R.id.etPasswort);
		btnOeffnen = (Button) findViewById(R.id.btnOeffnen);

		btnOeffnen.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int iPasswort = 42;
				String strEingabe = etPasswort.getText().toString();

				if(TextUtils.isEmpty(strEingabe))  {
					Toast.makeText(MyActivity.this, "Gebe das Passwort ein.", Toast.LENGTH_LONG).show();
				} else {
					if(iPasswort == Integer.parseInt(strEingabe)) {
						ivBild.setVisibility(View.VISIBLE);
					} else {
						Toast.makeText(MyActivity.this, "Das Passwort ist falsch!", Toast.LENGTH_LONG).show();
					}
				}
			}
		});
    }
}
